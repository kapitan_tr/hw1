/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.view;

import Employee.utils.EmployeeDataTableModel;
import Employee.utils.EmployeeFunction;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import phucldh.dao.AccountDAO;
import phucldh.dao.EmployeeDAO;
import phucldh.dto.EmployeeDTO;

/**
 *
 * @author Admin
 */
public class MainForm extends javax.swing.JDialog {

    /**
     * Creates new form Main
     */
    LoginForm p;
    private int roles = 0;
    public String filename = "employee.txt";
    public EmployeeDataTableModel<EmployeeDTO> model;
    public EmployeeDataTableModel<EmployeeDTO> modelling;
    public EmployeeDataTableModel<EmployeeDTO> modelAfter;
    public boolean addNew = false;
    public boolean search = false;

    public MainForm(java.awt.Frame parent, boolean modal) throws Exception {
        super(parent, modal);
        initComponents();
        p = (LoginForm) parent;
        if (p.fullName.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Login fail please try again");
            p.setVisible(true);
        } else {
            AccountDAO dao = new AccountDAO();
            roles = dao.getRoleAccount(p.fullName);
            if (roles == 0) {
                btnCreate.setEnabled(false);
                btnDelete.setEnabled(false);
                btnUpdate.setEnabled(false);
            }
            p.dispose();
        }
        int indexes[] = {0, 1, 2, 3};
        String header[] = {"EmpID", "Fullname", "Phone", "Email"};
        model = new EmployeeDataTableModel<EmployeeDTO>(header, indexes);
        this.tblEmployee.setModel(model);
        loadData();
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        tblEmployee.getTableHeader().setReorderingAllowed(false);
    }

    public void loadData() {
        EmployeeFunction function = new EmployeeFunction();
        try {
            List<EmployeeDTO> list = function.findAllArmor();
            for (EmployeeDTO employee : list) {
                model.getData().add(employee);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadDataAfterDelete() {
        try {
            FileReader f = new FileReader(filename);
            BufferedReader br = new BufferedReader(f);
            String s;
            boolean isDelete;
            while ((s = br.readLine()) != null) {
                s = s.trim();
                if (s.length() > 0) {
                    StringTokenizer stk = new StringTokenizer(s, ";");
                    String delete = stk.nextToken();
                    if (delete.equalsIgnoreCase("true")) {
                        isDelete = true;
                        String empId = stk.nextToken();
                        String fullName = stk.nextToken();
                        String phone = stk.nextToken();
                        String email = stk.nextToken();
                        String address = stk.nextToken();
                        String date = stk.nextToken();
                        Date dayOfBirth = new SimpleDateFormat("mm/dd/yyyy").parse(date);
                    } else if (delete.equalsIgnoreCase("false")) {
                        isDelete = false;
                        String empId = stk.nextToken();
                        String fullName = stk.nextToken();
                        String phone = stk.nextToken();
                        String email = stk.nextToken();
                        String address = stk.nextToken();
                        String date = stk.nextToken();
                        Date dayOfBirth = new SimpleDateFormat("mm/dd/yyyy").parse(date);
                        modelAfter.getData().add(new EmployeeDTO(isDelete, empId, fullName, phone, email, address, dayOfBirth));
                    }
                }
            }
            br.close();
            f.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean containsIgnoreCase(String s, String subString) {
        return (s.toLowerCase().contains(subString.toLowerCase()));
    }

    public void deleteAllRows(EmployeeDataTableModel<EmployeeDTO> model) {
        while (model.getRowCount() > 0) {
            model.removeRow(model.getRowCount() - 1);
        }
    }

    public String getDayOfBirth(Date dateOfBirth) {
        String day = dateOfBirth.toString().substring(14, 16);
        String month = dateOfBirth.toString().substring(8, 10);
        String year = dateOfBirth.toString().substring(24, 28);
        String dob = month + "/" + day + "/" + year;
        return dob;
    }

    private boolean validData() {
        String code = txtEmpID.getText();
        String name = txtFullname.getText();
        String address = txtAddress.getText();
        String phone = txtPhone.getText();
        String email = txtEmail.getText();
        String dob = txtDOB.getText();
          
        if (code.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Must input code");
            txtEmpID.requestFocus();
            return false;
        }
        if (!code.matches("[a-zA-Z0-9\\s]{1,10}")) {
            JOptionPane.showMessageDialog(this, "Invalid code");
            txtEmpID.requestFocus();
            return false;
        }
        if (name.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Must input name");
            txtEmpID.requestFocus();
            return false;
        }
        if (!name.matches("[a-zA-Z0-9\\s]{1,30}")) {
            JOptionPane.showMessageDialog(this, "Invalid name");
            txtFullname.requestFocus();
            return false;
        }
        if (phone.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Must input phone");
            txtEmpID.requestFocus();
            return false;
        }
        if (!phone.matches("\\d{10,15}")) {
            JOptionPane.showMessageDialog(this, "Invalid phone");
            txtPhone.requestFocus();
            return false;
        }
        if (email.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Must input email");
            txtEmpID.requestFocus();
            return false;
        }
        if (!email.matches("[\\w+@\\w+[.]\\w+]{1,30}")) {
            JOptionPane.showMessageDialog(this, "Invalid email");
            txtEmail.requestFocus();
            return false;
        }
        if (address.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Must input address");
            txtEmpID.requestFocus();
            return false;
        }
        if (!address.matches("[a-zA-Z0-9\\s]{1,300}")) {
            JOptionPane.showMessageDialog(this, "Invalid address");
            txtAddress.requestFocus();
            return false;
        }
        int month = Integer.parseInt(dob.substring(0, 2));
        int day = Integer.parseInt(dob.substring(3, 5));
        int year = Integer.parseInt(dob.substring(6, 10)); 
        if (month < 1 || month > 12) {
            JOptionPane.showMessageDialog(this, "Invalid month");
            return false;
        }
        if (day < 1 || day > 31) {
            JOptionPane.showMessageDialog(this, "Invalid day");
            return false;
        }
        if (year < 1 || month > 9999) {
            JOptionPane.showMessageDialog(this, "Invalid year");
            return false;
        }
        return true;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblEmployee = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtEmpID = new javax.swing.JTextField();
        btnFindByID = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txtFullname = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtPhone = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtAddress = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        txtDOB = new javax.swing.JTextField();
        btnCreate = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnLogout = new javax.swing.JButton();
        btnGetAll = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 255));
        jLabel1.setText("Emp Management");

        tblEmployee.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblEmployee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEmployeeMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblEmployee);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Emp's Details"));

        jLabel2.setText("EmpID:");

        btnFindByID.setText("Find By EmpID");
        btnFindByID.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindByIDActionPerformed(evt);
            }
        });

        jLabel3.setText("Fullname:");

        jLabel4.setText("Phone:");

        jLabel5.setText("Email:");

        jLabel6.setText("Address");
        jLabel6.setToolTipText("");

        txtAddress.setColumns(20);
        txtAddress.setRows(5);
        jScrollPane2.setViewportView(txtAddress);

        jLabel7.setText("DOB:");

        btnCreate.setText("Create Emp");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        btnUpdate.setText("Update Emp");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete Emp");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtDOB)
                            .addComponent(txtEmpID)
                            .addComponent(txtFullname)
                            .addComponent(txtPhone)
                            .addComponent(txtEmail)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 101, Short.MAX_VALUE)
                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 93, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnFindByID, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(24, 24, 24))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtEmpID)
                    .addComponent(btnFindByID, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtFullname, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtPhone, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtDOB, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCreate, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnDelete, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(91, Short.MAX_VALUE))
        );

        btnLogout.setText("Logout");
        btnLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLogoutActionPerformed(evt);
            }
        });

        btnGetAll.setText("Get All Employee");
        btnGetAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGetAllActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(172, 172, 172)
                                .addComponent(btnGetAll, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(395, 395, 395)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLogout, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)
                    .addComponent(btnLogout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnGetAll, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        if (!validData()) {
            return;
        }
        try {
            EmployeeFunction function = new EmployeeFunction();
            int selectedIndex = tblEmployee.getSelectedRow();
            String empId = txtEmpID.getText().trim();
            String fullName = txtFullname.getText().trim();
            String phone = txtPhone.getText().trim();
            String email = txtEmail.getText().trim();
            String address = txtAddress.getText().trim();
            String date = txtDOB.getText().trim();
            Date dayOfBirth = new SimpleDateFormat("mm/dd/yyyy").parse(date);
            boolean isDelete = false;
            EmployeeDTO emp = new EmployeeDTO(isDelete, empId, fullName, phone, email, address, dayOfBirth);
            function.updateArmor(emp);
            if (selectedIndex >= 0) {
                model.getData().set(selectedIndex, emp);
            }
            tblEmployee.updateUI();
            this.txtEmpID.setText("");
            this.txtFullname.setText("");
            this.txtPhone.setText("");
            this.txtEmail.setText("");
            this.txtAddress.setText("");
            this.txtDOB.setText("");
        } catch (ParseException ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        if (!validData()) {
            return;
        }
        try {
            addNew = true;
            int selectedIndex = tblEmployee.getSelectedRow();
            String empId = txtEmpID.getText().trim();
            String fullName = txtFullname.getText().trim();
            String phone = txtPhone.getText().trim();
            String email = txtEmail.getText().trim();
            String address = txtAddress.getText().trim();
            String date = txtDOB.getText().trim();
            Date dayOfBirth = new SimpleDateFormat("mm/dd/yyyy").parse(date);
            boolean isDelete = false;
            EmployeeDTO emp = new EmployeeDTO(isDelete, empId, fullName, phone, email, address, dayOfBirth);
            EmployeeFunction function = new EmployeeFunction();
            function.createArmor(emp);
            if (addNew && selectedIndex < 0) {
                model.getData().add(emp);
                tblEmployee.updateUI();
                addNew = false;
            }
            this.txtEmpID.setText("");
            this.txtFullname.setText("");
            this.txtPhone.setText("");
            this.txtEmail.setText("");
            this.txtAddress.setText("");
            this.txtDOB.setText("");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }//GEN-LAST:event_btnCreateActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblEmployee.getSelectedRow();
        try {
            if (JOptionPane.showConfirmDialog(this, "Are you sure?", "remove?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION && selectedIndex != -1) {
                model.getData().get(selectedIndex).setIsDelete(true);
                EmployeeFunction function = new EmployeeFunction();
                function.removeArmor(model.getData().get(selectedIndex).getEmpID());
                model.getData().remove(selectedIndex);
                tblEmployee.updateUI();
            } else {
                JOptionPane.showMessageDialog(null, "Choose row first!");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnFindByIDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindByIDActionPerformed
        int[] index = {0, 1, 2, 3};
        String idToSearch = txtEmpID.getText();
        if (idToSearch.equalsIgnoreCase("")) {
            JOptionPane.showMessageDialog(this, "Must input id");
            txtEmpID.requestFocus();
            return;
        }
        String[] header = {"EmpID", "Fullname", "Phone", "Email"};
        if (!txtEmpID.getText().isEmpty()) {
            search = true;
            modelling = new EmployeeDataTableModel<>(header, index);
            EmployeeDTO employee = null;
            try {
                EmployeeFunction function = new EmployeeFunction();
                employee = function.findByArmorID(idToSearch);
                if (employee != null) {
                    modelling.getData().add(employee);
                    this.tblEmployee.setModel(modelling);
                } else if (employee == null) {
                    JOptionPane.showMessageDialog(this, "Employee does not exist");
                    return;
                }
            } catch (Exception e) {
                System.out.println(e);
            }
            txtFullname.setEditable(false);
            txtPhone.setEditable(false);
            txtEmail.setEditable(false);
            txtAddress.setEditable(false);
            txtDOB.setEditable(false);
            btnUpdate.setEnabled(false);
            btnDelete.setEnabled(false);
            btnCreate.setEnabled(false);
        } else {
            search = false;
            deleteAllRows(modelling);
            this.tblEmployee.setModel(model);
            txtEmpID.setEditable(true);
            txtFullname.setEditable(true);
            txtPhone.setEditable(true);
            txtEmail.setEditable(true);
            txtAddress.setEditable(true);
            txtDOB.setEditable(true);
            btnUpdate.setEnabled(true);
            btnDelete.setEnabled(true);
            btnCreate.setEnabled(true);
        }
        tblEmployee.updateUI();
    }//GEN-LAST:event_btnFindByIDActionPerformed

    private void tblEmployeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEmployeeMouseClicked
        int pos = tblEmployee.getSelectedRow();
        EmployeeDTO empCurrent = model.getData().get(pos);
        this.txtEmpID.setText(empCurrent.getEmpID());
        this.txtFullname.setText(empCurrent.getFullname());
        this.txtPhone.setText(empCurrent.getPhone());
        this.txtEmail.setText(empCurrent.getEmail());
        this.txtAddress.setText(empCurrent.getAddress());
        String dob = getDayOfBirth(empCurrent.getDateOfBirth());
        this.txtDOB.setText(dob);
        this.setEnabled(true);
        this.addNew = false;
        txtEmpID.setEditable(false);
        boolean a = tblEmployee.isEditing();
        if (a == false) {
            JOptionPane.showMessageDialog(null, "You can't edit on this table");
        }
    }//GEN-LAST:event_tblEmployeeMouseClicked

    private void btnLogoutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLogoutActionPerformed
        p.fullName = null;
        LoginForm login = new LoginForm();
        login.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnLogoutActionPerformed

    private void btnGetAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGetAllActionPerformed
        tblEmployee.updateUI();
        this.tblEmployee.setModel(model);
        this.txtEmpID.setText("");
        this.txtFullname.setText("");
        this.txtPhone.setText("");
        this.txtEmail.setText("");
        this.txtAddress.setText("");
        this.txtDOB.setText("");
        txtEmpID.setEditable(true);
        txtFullname.setEditable(true);
        txtPhone.setEditable(true);
        txtEmail.setEditable(true);
        txtAddress.setEditable(true);
        txtDOB.setEditable(true);
        btnUpdate.setEnabled(true);
        btnDelete.setEnabled(true);
        btnCreate.setEnabled(true);
    }//GEN-LAST:event_btnGetAllActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainForm dialog = new MainForm(new javax.swing.JFrame(), true);
                    dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                        @Override
                        public void windowClosing(java.awt.event.WindowEvent e) {
                            System.exit(0);
                        }
                    });
                    dialog.setVisible(true);
                } catch (Exception ex) {
                    Logger.getLogger(MainForm.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreate;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnFindByID;
    private javax.swing.JButton btnGetAll;
    private javax.swing.JButton btnLogout;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tblEmployee;
    private javax.swing.JTextArea txtAddress;
    private javax.swing.JTextField txtDOB;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEmpID;
    private javax.swing.JTextField txtFullname;
    private javax.swing.JTextField txtPhone;
    // End of variables declaration//GEN-END:variables
}
